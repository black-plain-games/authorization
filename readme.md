# Getting Started

1. Build the solution
1. Publish the `BlackPlain.Authorization.Database` project by double-clicking the `local.publish.xml` file
1. Add `127.0.0.1	authorization.local` to your hosts file
1. Add a new site to IIS called `authorization`
  1. Edit the only existing binding, giving it a host name of `authorization.local`
1. In SSMS, right click `Security` > `Logins` and add a new Login
  1. Name it `IIS APPPOOL\authorization`
  1. Under `User Mapping`, check off `BlackPlain`
  1 Also check off `db_datareader` and `db_datawriter`
  
# Troubleshooting

## Build Errors

### `Missing reference to EntityFramework`
1. Install `EntityFramework` via `Manage Nuget Packages for Solution` to the following projects
  1. `BlackPlain.Authorization.Entities`
  1. `BlackPlain.Authorization.Repositories`
  
## Runtime Errors

### `Could not load file or assembly "System.Net.Http"`
1. In the `web.config` for the `BlackPlain.Authorization` project, verify that the `runtime.assemblyBinding.dependentAssembly` node for `System.Net.Http` does not contain a child `bindingRedirect` node

### Nothing