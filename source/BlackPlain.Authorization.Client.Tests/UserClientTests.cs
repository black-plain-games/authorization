﻿using FluentAssertions;
using BlackPlain.Core.Services;
using BlackPlain.Core.Web;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BlackPlain.Authorization.Client.Tests
{
    [TestClass]
    public class UserClientTests
    {
        private IAuthorizationClient _authorizationClient;

        private IAuthorizationClient GetAuthorizationClient() => new AuthorizationClient(new AppConfigSettingService());

        [TestInitialize]
        public void Initialize()
        {
            _authorizationClient = GetAuthorizationClient();
        }

        [TestMethod]
        public void Ping_succeeds()
        {
            var response = _authorizationClient.UserClient.Ping("test");

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNullOrWhiteSpace();

            response.Content.Should().NotBeNullOrWhiteSpace();
        }
    }
}