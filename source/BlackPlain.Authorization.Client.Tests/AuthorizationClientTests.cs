﻿using BlackPlain.Core.Services;
using BlackPlain.Core.Web;
using FluentAssertions;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;

namespace BlackPlain.Authorization.Client.Tests
{
    [TestClass]
    public class AuthorizationClientTests
    {
        private readonly string _username = "registered_user_1";

        private readonly string _password = "password1";

        private readonly int _userId = 10000;

        private IAuthorizationClient _authorizationClient;

        private IAuthorizationClient GetAuthorizationClient() => new AuthorizationClient(new AppConfigSettingService());

        [TestInitialize]
        public void Initialize()
        {
            _authorizationClient = GetAuthorizationClient();

            if (File.Exists(".refresh"))
            {
                File.Delete(".refresh");
            }
        }

        [TestMethod]
        public void Login_fails_with_missing_username()
        {
            var response = _authorizationClient.Login(null, "123");

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.NotSent);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Login_fails_with_missing_password()
        {
            var response = _authorizationClient.Login("123", null);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.NotSent);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Login_fails_with_bad_credentials()
        {
            var response = _authorizationClient.Login("123", "123");

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.BadRequest);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Login_succeeds_with_valid_credentials()
        {
            var response = _authorizationClient.Login(_username, _password);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNull();

            _authorizationClient.AccessToken.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Authorize_fails_before_login()
        {
            var response = _authorizationClient.Authorize();

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.Unauthorized);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Authorize_fails_with_invalid_refresh_token()
        {
            using (var stream = File.Create(".refresh"))
            using (var writer = new StreamWriter(stream))
            {
                writer.Write("abcdefghijklmnopqrstuvwxyz");
            }

            var response = _authorizationClient.Authorize();

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.BadRequest);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Authorize_succeeds_after_login()
        {
            _authorizationClient.Login(_username, _password);

            var response = _authorizationClient.Authorize();

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNull();

            _authorizationClient.AccessToken.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Authorize_succeeds_with_refresh_token()
        {
            _authorizationClient.Login(_username, _password);

            _authorizationClient.Authorize();

            _authorizationClient = GetAuthorizationClient();

            var Authorize = _authorizationClient.Authorize();

            Authorize.Should().NotBeNull();

            Authorize.Code.Should().Be(ResponseCode.OK);

            Authorize.Message.Should().BeNull();
        }

        [TestMethod]
        public void Authorized_request_fails_before_login()
        {
            var response = _authorizationClient.UserClient.Retrieve("asd", 10001);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.Unauthorized);

            response.Message.Should().NotBeNullOrWhiteSpace();

            response.Content.Should().BeNull();
        }

        [TestMethod]
        public void Authorized_request_suceeds_after_login()
        {
            _authorizationClient.Login(_username, _password);

            var Authorize = _authorizationClient.Authorize();

            Authorize.Should().NotBeNull();

            Authorize.Code.Should().Be(ResponseCode.OK);

            Authorize.Message.Should().BeNull();

            var response = _authorizationClient.UserClient.Retrieve(_authorizationClient.AccessToken, _userId);

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNull();

            response.Content.Should().NotBeNull();

            _authorizationClient.AccessToken.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Logout_fails_when_not_logged_in()
        {
            var response = _authorizationClient.Logout();

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.NotSent);

            response.Message.Should().NotBeNullOrWhiteSpace();
        }

        [TestMethod]
        public void Logout_succeeds_when_logged_in()
        {
            _authorizationClient.Login(_username, _password);

            var response = _authorizationClient.Logout();

            response.Should().NotBeNull();

            response.Code.Should().Be(ResponseCode.OK);

            response.Message.Should().BeNull();
        }
    }
}