﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class RefreshTokenRepository : IRefreshTokenRepository
    {
        public RefreshTokenRepository(Entities.BlackPlainEntities context)
        {
            _context = context;
        }

        public async Task<DbResult> Create(RefreshToken refreshToken)
        {
            var existingToken = _context.RefreshTokens.SingleOrDefault(rt => rt.UserId == refreshToken.UserId && rt.ClientId == refreshToken.ClientId);

            if (existingToken != null)
            {
                if (!(await Delete(existingToken)).IsSuccess)
                {
                    var messages = new[]
                    {
                        $"Unable to delete refresh token for user {refreshToken.UserId} and client {refreshToken.ClientId}."
                    };

                    return new DbResult(false, messages);
                }
            }

            _context.RefreshTokens.Add(new Entities.RefreshToken
            {
                Id = refreshToken.Id,
                UserId = refreshToken.UserId,
                ClientId = refreshToken.ClientId,
                IssueDate = refreshToken.IssueDate,
                ExpirationDate = refreshToken.ExpirationDate,
                ProtectedTicket = refreshToken.ProtectedTicket
            });

            var result = await _context.SaveChangesAsync() > 0;

            return new DbResult(result);
        }

        public async Task<string> Retrieve(string refreshTokenId)
        {
            var dbRefreshToken = await _context.RefreshTokens.FindAsync(refreshTokenId);

            if (dbRefreshToken == null)
            {
                return null;
            }

            if (dbRefreshToken.ExpirationDate < DateTimeOffset.Now)
            {
                return null;
            }

            return dbRefreshToken.ProtectedTicket;
        }

        public async Task<DbResult> Delete(string refreshTokenId)
        {
            var refreshToken = await _context.RefreshTokens.FindAsync(refreshTokenId);

            if (refreshToken != null)
            {
                return await Delete(refreshToken);
            }

            return new DbResult(false);
        }

        private async Task<DbResult> Delete(Entities.RefreshToken dbRefreshToken)
        {
            _context.RefreshTokens.Remove(dbRefreshToken);

            var result = await _context.SaveChangesAsync() > 0;

            return new DbResult(result);
        }

        private readonly Entities.BlackPlainEntities _context;
    }
}