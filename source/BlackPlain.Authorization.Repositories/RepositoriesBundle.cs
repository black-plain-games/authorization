﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.DI;
using Microsoft.AspNet.Identity;

namespace BlackPlain.Authorization.Repositories
{
    public class RepositoriesBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<Entities.BlackPlainEntities, Entities.BlackPlainEntities>();

            hub.CreateResolver<IUserStore<User, int>, UserStore>();

            hub.CreateResolver<IUserManager, UserManager>();

            hub.CreateResolver<IUserRepository, UserRepository>();

            hub.CreateResolver<IRoleStore<Role, byte>, RoleStore>();

            hub.CreateResolver<IRoleManager, RoleManager>();

            hub.CreateResolver<IRoleRepository, RoleRepository>();

            hub.CreateResolver<IClientRepository, ClientRepository>();

            hub.CreateResolver<IUserTokenRepository, UserTokenRepository>();

            hub.CreateResolver<IRefreshTokenRepository, RefreshTokenRepository>();
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}