﻿using BlackPlain.Authorization.Core;
using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class RoleStore : IRoleStore<Role, byte>
    {
        public RoleStore(Entities.BlackPlainEntities context)
        {
            _context = context;
        }

        public async void Dispose()
        {
        }

        public async Task CreateAsync(Role role)
        {
            var dbRole = new Entities.Role
            {
                Name = role.Name
            };

            _context.Roles.Add(dbRole);

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(Role role)
        {
            var dbRole = await FindDbRole(role.Id);

            _context.Roles.Remove(dbRole);
        }

        public async Task<Role> FindByIdAsync(byte roleId)
        {
            var result = await FindDbRole(roleId);

            return Convert(result);
        }

        public async Task<Role> FindByNameAsync(string roleName)
        {
            var result = _context.Roles.FirstOrDefault(r => r.Name == roleName);

            if (result == null)
            {
                throw new ArgumentOutOfRangeException(nameof(roleName));
            }

            return Convert(result);
        }

        public async Task UpdateAsync(Role role)
        {
            var dbRole = await FindDbRole(role.Id);

            dbRole.Name = role.Name;

            await _context.SaveChangesAsync();
        }

        private static Role Convert(Entities.Role dbRole)
        {
            return new Role
            {
                Id = dbRole.Id,
                Name = dbRole.Name
            };
        }

        private async Task<Entities.Role> FindDbRole(byte roleId)
        {
            var result = await _context.Roles.FindAsync(roleId);

            if (result == null)
            {
                throw new ArgumentOutOfRangeException(nameof(roleId));
            }

            return result;
        }

        private readonly Entities.BlackPlainEntities _context;
    }
}