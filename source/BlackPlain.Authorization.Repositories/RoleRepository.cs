﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class RoleRepository : IRoleRepository
    {
        public RoleRepository(IRoleManager roleManager)
        {
            _roleManager = roleManager;
        }

        public async Task<Role> Retrieve(byte roleId)
        {
            return await _roleManager.FindByIdAsync(roleId);
        }

        private readonly IRoleManager _roleManager;
    }
}