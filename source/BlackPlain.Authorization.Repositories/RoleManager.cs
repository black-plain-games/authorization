﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using Microsoft.AspNet.Identity;

namespace BlackPlain.Authorization.Repositories
{
    public class RoleManager : RoleManager<Role, byte>, IRoleManager
    {
        public RoleManager(IRoleStore<Role, byte> store)
            : base(store)
        {
        }
    }
}