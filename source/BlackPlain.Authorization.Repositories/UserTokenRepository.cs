﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class UserTokenRepository : IUserTokenRepository
    {
        public UserTokenRepository(Entities.BlackPlainEntities context)
        {
            _context = context;
        }

        public async Task<string> Create(int userId, TokenPurpose purpose)
        {
            var existingToken = _context.UserTokens.FirstOrDefault(ut => ut.UserId == userId && ut.TokenPurposeId == (byte)purpose);

            if (existingToken == null)
            {
                existingToken = new Entities.UserToken
                {
                    UserId = userId,
                    TokenPurposeId = (byte)purpose,
                    DateCreated = DateTimeOffset.UtcNow,
                    DateModified = DateTimeOffset.UtcNow
                };

                _context.UserTokens.Add(existingToken);
            }

            existingToken.IsValid = false;

            existingToken.Token = Guid.NewGuid().ToString("N");

            await _context.SaveChangesAsync();

            return existingToken.Token;
        }

        public async Task<bool> Validate(int userId, TokenPurpose purpose, string token)
        {
            var existingToken = _context.UserTokens.FirstOrDefault(ut => ut.UserId == userId && ut.TokenPurposeId == (byte)purpose && !ut.IsValid);

            if (existingToken == null)
            {
                return false;
            }

            if (existingToken.Token != token)
            {
                return false;
            }

            existingToken.DateModified = DateTimeOffset.UtcNow;

            existingToken.IsValid = true;

            return await _context.SaveChangesAsync() > 0;
        }

        private readonly Entities.BlackPlainEntities _context;
    }
}