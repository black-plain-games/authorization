﻿using BlackPlain.Authorization.Core.Repositories;
using Microsoft.AspNet.Identity;
using System.Linq;

namespace BlackPlain.Authorization.Repositories
{
    internal static class Helpers
    {
        public static DbResult Convert(IdentityResult dbResult)
        {
            if (dbResult == null)
            {
                return new DbResult(false);
            }

            return new DbResult(dbResult.Succeeded, dbResult.Errors.ToList());
        }

        public static Core.Ban Convert(Entities.Ban dbBan)
        {
            return new Core.Ban
            {
                Id = dbBan.Id,
                ClientId = dbBan.ClientId,
                DateCreated = dbBan.DateCreated,
                StartDate = dbBan.StartDate,
                EndDate = dbBan.EndDate,
                Details = dbBan.Details
            };
        }
    }
}