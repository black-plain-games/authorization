﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using System;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class ClientRepository : IClientRepository
    {
        public ClientRepository(Entities.BlackPlainEntities context)
        {
            _context = context;
        }

        public async Task<int> Create(Client client)
        {
            var dbClient = new Entities.Client
            {
                Name = client.Name,
                IsActive = false,
                RefreshTokenLifeTimeMinutes = (int)client.RefreshTokenLifeTime.TotalMinutes,
                ApplicationTypeId = (byte)ApplicationType.NativeConfidential,
                AllowedOrigin = client.AllowedOrigin,
                Secret = client.Secret
            };

            _context.Clients.Add(dbClient);

            await _context.SaveChangesAsync();

            return dbClient.Id;
        }

        public async Task<Client> Retrieve(int clientId)
        {
            var dbClient = await _context.Clients.FindAsync(clientId);

            if (dbClient == null)
            {
                return null;
            }

            return new Client(dbClient.Id, dbClient.DateCreated)
            {
                Name = dbClient.Name,
                IsActive = dbClient.IsActive,
                Secret = dbClient.Secret,
                RefreshTokenLifeTime = TimeSpan.FromMinutes(dbClient.RefreshTokenLifeTimeMinutes),
                AllowedOrigin = dbClient.AllowedOrigin
            };
        }

        public async Task Update(Client client)
        {
            var dbClient = await _context.Clients.FindAsync(client.Id);

            if (dbClient == null)
            {
                return;
            }

            dbClient.AllowedOrigin = client.AllowedOrigin;

            dbClient.IsActive = client.IsActive;

            dbClient.Name = client.Name;

            dbClient.RefreshTokenLifeTimeMinutes = (int)client.RefreshTokenLifeTime.TotalMinutes;

            if (!string.IsNullOrWhiteSpace(client.Secret))
            {
                dbClient.Secret = client.Secret;
            }

            await _context.SaveChangesAsync();
        }

        private readonly Entities.BlackPlainEntities _context;
    }
}