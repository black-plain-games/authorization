﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using Microsoft.AspNet.Identity;

namespace BlackPlain.Authorization.Repositories
{
    public class UserManager : UserManager<User, int>, IUserManager
    {
        public UserManager(IUserStore<User, int> userStore,
            IUserTokenProvider<User, int> userTokenProvider)
            : base(userStore)
        {
            UserTokenProvider = userTokenProvider;
        }
    }
}