﻿using BlackPlain.Authorization.Core;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class UserStore :
        IUserStore<User, int>,
        IUserPasswordStore<User, int>,
        IUserRoleStore<User, int>,
        IUserEmailStore<User, int>,
        IUserPhoneNumberStore<User, int>,
        IQueryableUserStore<User, int>
    {
        public IQueryable<User> Users
        {
            get
            {
                return _context.Users.ToList().Select(u => Convert(u)).AsQueryable();
            }
        }

        public UserStore(Entities.BlackPlainEntities context)
        {
            _context = context;
        }

        public void Dispose()
        {
        }

        public async Task CreateAsync(User user)
        {
            var dbUser = new Entities.User
            {
                UserName = user.UserName,
                PasswordHash = user.PasswordHash,
                FirstName = user.FirstName,
                LastName = user.LastName
            };

            if (!string.IsNullOrWhiteSpace(user.EmailAddress))
            {
                dbUser.EmailAddress = user.EmailAddress;

                dbUser.IsEmailAddressVerified = false;
            }

            if (!string.IsNullOrWhiteSpace(user.PhoneNumber))
            {
                dbUser.PhoneNumber = user.PhoneNumber;

                dbUser.IsPhoneNumberVerified = false;
            }

            _context.Users.Add(dbUser);

            await _context.SaveChangesAsync();
        }

        public async Task<User> FindByIdAsync(int userId)
        {
            var result = _context.Users.FirstOrDefault(u => u.Id == userId);

            if (result == null)
            {
                return null;
            }

            return Convert(result);
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            var result = _context.Users.FirstOrDefault(u => u.UserName == userName);

            if (result == null)
            {
                return null;
            }

            return Convert(result);
        }

        public async Task UpdateAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            dbUser.UserName = user.UserName;

            dbUser.DateModified = DateTimeOffset.Now;

            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            _context.Users.Remove(dbUser);

            await _context.SaveChangesAsync();
        }

        public async Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;

            var dbUser = await FindDbUser(user.Id);

            if (dbUser != null)
            {
                dbUser.PasswordHash = passwordHash;

                await _context.SaveChangesAsync();
            }
        }

        public async Task<string> GetPasswordHashAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.PasswordHash;
        }

        public async Task<bool> HasPasswordAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return !string.IsNullOrWhiteSpace(dbUser.PasswordHash);
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            var dbUser = await FindDbUser(user.Id);

            var dbRole = FindDbRole(roleName);

            dbUser.Roles.Add(dbRole);

            await _context.SaveChangesAsync();
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            var dbUser = await FindDbUser(user.Id);

            var dbRole = dbUser.Roles.FirstOrDefault(r => r.Name == roleName);

            if (dbRole == null)
            {
                throw new ArgumentOutOfRangeException(nameof(roleName));
            }

            dbUser.Roles.Remove(dbRole);

            await _context.SaveChangesAsync();
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.Roles.Select(r => r.Name).ToList();
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.Roles.Any(r => r.Name == roleName);
        }

        public async Task SetEmailAsync(User user, string email)
        {
            var dbUser = await FindDbUser(user.Id);

            dbUser.EmailAddress = email;

            dbUser.IsEmailAddressVerified = false;

            await _context.SaveChangesAsync();
        }

        public async Task<string> GetEmailAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.EmailAddress;
        }

        public async Task<bool> GetEmailConfirmedAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.IsEmailAddressVerified ?? false;
        }

        public async Task SetEmailConfirmedAsync(User user, bool confirmed)
        {
            var dbUser = await FindDbUser(user.Id);

            dbUser.IsEmailAddressVerified = confirmed;

            await _context.SaveChangesAsync();
        }

        public async Task<User> FindByEmailAsync(string email)
        {
            var dbUser = _context.Users.FirstOrDefault(u => u.EmailAddress == email);

            if (dbUser == null)
            {
                return null;
            }

            return Convert(dbUser);
        }

        public async Task SetPhoneNumberAsync(User user, string phoneNumber)
        {
            var dbUser = await FindDbUser(user.Id);

            dbUser.PhoneNumber = phoneNumber;

            await _context.SaveChangesAsync();
        }

        public async Task<string> GetPhoneNumberAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.PhoneNumber;
        }

        public async Task<bool> GetPhoneNumberConfirmedAsync(User user)
        {
            var dbUser = await FindDbUser(user.Id);

            return dbUser.IsPhoneNumberVerified ?? false;
        }

        public async Task SetPhoneNumberConfirmedAsync(User user, bool confirmed)
        {
            var dbUser = await FindDbUser(user.Id);

            dbUser.IsPhoneNumberVerified = confirmed;

            await _context.SaveChangesAsync();
        }

        private static User Convert(Entities.User dbUser)
        {
            return new User
            {
                Id = dbUser.Id,
                DateCreated = dbUser.DateCreated,
                DateModified = dbUser.DateModified,
                UserName = dbUser.UserName,
                PasswordHash = dbUser.PasswordHash,
                EmailAddress = dbUser.EmailAddress,
                IsEmailAddressVerified = dbUser.IsEmailAddressVerified,
                PhoneNumber = dbUser.PhoneNumber,
                IsPhoneNumberVerified = dbUser.IsPhoneNumberVerified,
                FirstName = dbUser.FirstName,
                LastName = dbUser.LastName,
                RoleIds = dbUser.Roles.Select(r => r.Id).ToList(),
                Bans = dbUser.Bans.ToList().Select(b => Helpers.Convert(b))
            };
        }

        private async Task<Entities.User> FindDbUser(int userId)
        {
            return await _context.Users.FindAsync(userId);
        }

        private Entities.Role FindDbRole(string roleName)
        {
            return _context.Roles.FirstOrDefault(r => r.Name == roleName);
        }

        private readonly Entities.BlackPlainEntities _context;
    }
}