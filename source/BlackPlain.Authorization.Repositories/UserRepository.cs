﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Repositories
{
    public class UserRepository : IUserRepository
    {
        public UserRepository(IUserManager userManager)
        {
            _userManager = userManager;
        }

        public async Task<DbResult> Create(User user, string password)
        {
            var dbUser = await RetrieveByUserName(user.UserName);

            if (dbUser != null)
            {
                return new DbResult(false, "User name is taken");
            }

            dbUser = await RetrieveByEmailAddress(user.EmailAddress);

            if (dbUser != null)
            {
                return new DbResult(false, "User is already registered");
            }

            var dbResult = await _userManager.CreateAsync(user, password);

            return Helpers.Convert(dbResult);
        }

        public async Task<User> Retrieve(string userName, string password)
        {
            return await _userManager.FindAsync(userName, password);
        }

        public async Task<User> Retrieve(int userId)
        {
            return await _userManager.FindByIdAsync(userId);
        }

        public async Task<User> RetrieveByUserName(string userName)
        {
            return await _userManager.FindByNameAsync(userName);
        }

        public async Task<User> RetrieveByEmailAddress(string emailAddress)
        {
            return await _userManager.FindByEmailAsync(emailAddress);
        }

        public IEnumerable<User> Retrieve()
        {
            return _userManager.Users.ToList();
        }

        public async Task<string> GenerateEmailConfirmationToken(int userId)
        {
            return await _userManager.GenerateEmailConfirmationTokenAsync(userId);
        }

        public async Task<string> GeneratePasswordResetToken(int userId)
        {
            return await _userManager.GeneratePasswordResetTokenAsync(userId);
        }

        public async Task<DbResult> ConfirmEmail(int userId, string token)
        {
            var dbResult = await _userManager.ConfirmEmailAsync(userId, token);

            return Helpers.Convert(dbResult);
        }

        public async Task<DbResult> ResetPassword(int userId, string token, string password)
        {
            var dbResult = await _userManager.ResetPasswordAsync(userId, token, password);

            return Helpers.Convert(dbResult);
        }

        public async Task<DbResult> ChangePassword(int userId, string currentPassword, string newPassword)
        {
            var dbResult = await _userManager.ChangePasswordAsync(userId, currentPassword, newPassword);

            return Helpers.Convert(dbResult);
        }

        public async Task<DbResult> AddToRole(int userId, string role)
        {
            var dbResult = await _userManager.AddToRoleAsync(userId, role);

            return Helpers.Convert(dbResult);
        }

        private readonly IUserManager _userManager;
    }
}