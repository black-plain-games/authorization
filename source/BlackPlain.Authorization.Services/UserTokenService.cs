﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.Authorization.Core.Services;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Services
{
    public class UserTokenService : IUserTokenService
    {
        public UserTokenService(IUserTokenRepository userTokenRepository)
        {
            _userTokenRepository = userTokenRepository;
        }

        public async Task<string> Create(int userId, TokenPurpose purpose)
        {
            return await _userTokenRepository.Create(userId, purpose);
        }

        public async Task<bool> Validate(int userId, TokenPurpose purpose, string token)
        {
            return await _userTokenRepository.Validate(userId, purpose, token);
        }

        private readonly IUserTokenRepository _userTokenRepository;
    }
}