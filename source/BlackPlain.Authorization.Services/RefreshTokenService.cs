﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.Authorization.Core.Services;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Services
{
    public class RefreshTokenService : IRefreshTokenService
    {
        public RefreshTokenService(IRefreshTokenRepository refreshTokenRepository)
        {
            _refreshTokenRepository = refreshTokenRepository;
        }

        public async Task<bool> Create(RefreshToken refreshToken)
        {
            var dbResult = await _refreshTokenRepository.Create(refreshToken);

            return dbResult.IsSuccess;
        }

        public async Task<string> Retrieve(string refreshTokenId)
        {
            return await _refreshTokenRepository.Retrieve(refreshTokenId);
        }

        public async Task<bool> Delete(string refreshTokenId)
        {
            var result = await _refreshTokenRepository.Delete(refreshTokenId);

            return result.IsSuccess;
        }

        private readonly IRefreshTokenRepository _refreshTokenRepository;
    }
}