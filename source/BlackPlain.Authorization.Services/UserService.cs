﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.Authorization.Core.Services;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Services
{
    public class UserService : IUserService
    {
        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<int?> Create(User user, string password)
        {
            var createResult = await _userRepository.Create(user, password);

            if (!createResult.IsSuccess)
            {
                return null;
            }

            var retrieveResult = await _userRepository.Retrieve(user.UserName, password);

            return retrieveResult.Id;
        }

        public IEnumerable<User> Retrieve()
        {
            return _userRepository.Retrieve();
        }

        public async Task<User> Retrieve(int userId)
        {
            return await _userRepository.Retrieve(userId);
        }

        public async Task<User> Retrieve(string userName, string password)
        {
            return await _userRepository.Retrieve(userName, password);
        }

        public async Task<User> RetrieveByUserName(string userName)
        {
            return await _userRepository.RetrieveByUserName(userName);
        }

        public async Task<User> RetrieveByEmailAddress(string emailAddress)
        {
            return await _userRepository.RetrieveByEmailAddress(emailAddress);
        }

        public async Task<bool> AddToRole(int userId, string role)
        {
            var result = await _userRepository.AddToRole(userId, role);

            return result.IsSuccess;
        }

        public async Task<string> GenerateEmailConfirmationToken(int userId)
        {
            return await _userRepository.GenerateEmailConfirmationToken(userId);
        }

        public async Task<string> GeneratePasswordResetToken(int userId)
        {
            return await _userRepository.GeneratePasswordResetToken(userId);
        }

        public async Task<OperationResult> ConfirmEmail(int userId, string token)
        {
            var dbResult = await _userRepository.ConfirmEmail(userId, token);

            return new OperationResult(dbResult);
        }

        public async Task<OperationResult> ResetPassword(int userId, string token, string password)
        {
            var dbResult = await _userRepository.ResetPassword(userId, token, password);

            return new OperationResult(dbResult);
        }

        public async Task<OperationResult> ChangePassword(int userId, string currentPassword, string newPassword)
        {
            var dbResult = await _userRepository.ChangePassword(userId, currentPassword, newPassword);

            return new OperationResult(dbResult);
        }

        private readonly IUserRepository _userRepository;
    }
}