﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.Authorization.Core.Services;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Services
{
    public class ClientService : IClientService
    {
        public ClientService(IClientRepository clientRepository)
        {
            _clientRepository = clientRepository;
        }

        public async Task<int> Create(Client client)
        {
            client.Secret = CryptoHelper.GetHash(client.Secret);

            return await _clientRepository.Create(client);
        }

        public async Task<Client> Retrieve(int clientId)
        {
            return await _clientRepository.Retrieve(clientId);
        }

        public async Task Update(Client client)
        {
            if (!string.IsNullOrWhiteSpace(client.Secret))
            {
                client.Secret = CryptoHelper.GetHash(client.Secret);
            }

            await _clientRepository.Update(client);
        }

        private readonly IClientRepository _clientRepository;
    }
}