﻿using BlackPlain.Authorization.Core.Services;
using BlackPlain.Core.Services;
using System.ComponentModel;
using System.Net;
using System.Net.Mail;

namespace BlackPlain.Authorization.Services
{
    public class EmailService : IEmailService
    {
        public EmailService(ISettingService settingService)
        {
            _settingService = settingService;

            _client = new SmtpClient(_settingService["SmtpClientHost"], _settingService.Get<int>("SmtpClientPort"))
            {
                Credentials = new NetworkCredential(_settingService["SmtpClientUserName"], _settingService["SmtpClientPassword"]),
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network
            };

            _sender = new MailAddress(_settingService["EmailServiceFromAddress"], _settingService["EmailServiceDisplayName"]);

            _client.SendCompleted += OnSendCompleted;
        }

        public void SendEmail(string subject, string body, string toEmailAddress)
        {
            var email = new MailMessage
            {
                Subject = subject,
                Body = body,
                IsBodyHtml = true,
                DeliveryNotificationOptions = DeliveryNotificationOptions.OnSuccess,
                From = _sender,
                Sender = _sender
            };

            email.To.Add(new MailAddress(toEmailAddress));

            _client.Send(email);
        }

        private void OnSendCompleted(object sender, AsyncCompletedEventArgs e)
        {
        }

        private readonly ISettingService _settingService;

        private readonly SmtpClient _client;

        private readonly MailAddress _sender;
    }
}