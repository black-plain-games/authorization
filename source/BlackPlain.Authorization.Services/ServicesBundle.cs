﻿using BlackPlain.Authorization.Core.Services;
using BlackPlain.Core.Services;
using BlackPlain.DI;

namespace BlackPlain.Authorization.Services
{
    public class ServicesBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<IClientService, ClientService>();

            hub.CreateResolver<IRefreshTokenService, RefreshTokenService>();

            hub.CreateResolver<IRoleService, RoleService>();

            hub.CreateResolver<IUserService, UserService>();

            hub.CreateResolver<IEmailService, EmailService>();

            hub.CreateResolver<ISettingService, ApplicationSettingService>();

            hub.CreateResolver<IUserTokenService, UserTokenService>();
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}