﻿using BlackPlain.Core.Services;
using System;
using System.Configuration;
using System.Linq;

namespace BlackPlain.Authorization.Services
{
    public class ApplicationSettingService : ISettingService
    {
        public string this[string key] => Get(key);

        public string Get(string key)
        {
            return ConfigurationManager.AppSettings[key];
        }

        public string Get(string key, string fallbackValue)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
            {
                return Get(key);
            }

            return fallbackValue;
        }

        public T Get<T>(string key)
        {
            return (T)Convert.ChangeType(Get(key), typeof(T));
        }

        public T Get<T>(string key, T fallbackValue)
        {
            if (ConfigurationManager.AppSettings.AllKeys.Contains(key))
            {
                return Get<T>(key);
            }

            return fallbackValue;
        }
    }
}