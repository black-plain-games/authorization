﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Repositories;
using BlackPlain.Authorization.Core.Services;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Services
{
    public class RoleService : IRoleService
    {
        public RoleService(IRoleRepository roleRepository)
        {
            _roleRepository = roleRepository;
        }

        public async Task<Role> Retrieve(byte roleId)
        {
            return await _roleRepository.Retrieve(roleId);
        }

        private readonly IRoleRepository _roleRepository;
    }
}