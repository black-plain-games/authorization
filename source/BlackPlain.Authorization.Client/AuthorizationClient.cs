﻿using BlackPlain.Authorization.Web;
using BlackPlain.Core.Services;
using BlackPlain.Core.Web;
using RestSharp;
using System;
using System.IO;
using System.Net;

namespace BlackPlain.Authorization.Client
{
    public class AuthorizationClient : ClientBase, IAuthorizationClient
    {
        public IUserClient UserClient => _userClient;

        public string AccessToken => _accessToken;

        public int? UserId => _userId;

        public AuthorizationClient(ISettingService settingService)
            : base(new RestClient(settingService["AuthorizationBaseUrl"]), "api")
        {
            _clientId = settingService.Get<int>("ClientId");

            _clientSecret = settingService["ClientSecret"];

            _userClient = new UserClient(Client);
        }

        public void LoadRefreshToken()
        {
            _accessToken = null;

            _expirationDate = null;

            if (!File.Exists(".refresh"))
            {
                return;
            }

            _refreshToken = File.ReadAllText(".refresh");
        }

        public Response Authorize()
        {
            if (!string.IsNullOrWhiteSpace(_accessToken) &&
                (_expirationDate.HasValue && _expirationDate.Value > DateTimeOffset.Now))
            {
                return Ok();
            }

            if (string.IsNullOrWhiteSpace(_refreshToken))
            {
                LoadRefreshToken();

                if (string.IsNullOrWhiteSpace(_refreshToken))
                {
                    return new Response(ResponseCode.Unauthorized, "User is not logged in");
                }
            }

            var response = GetToken(new GetTokenRequest(_clientId, _clientSecret, _refreshToken));

            _accessToken = null;

            _expirationDate = null;

            _refreshToken = null;

            if (response.Code != ResponseCode.OK)
            {
                return new Response(response.Code, response.Message);
            }

            SaveGetTokenResponse(response.Content.Value);

            return Ok();
        }

        public Response Login(string username, string password)
        {
            if (string.IsNullOrWhiteSpace(username))
            {
                return new Response(ResponseCode.NotSent, "Username is null or empty");
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                return new Response(ResponseCode.NotSent, "Password is null or empty");
            }

            var tokenResponse = GetToken(new GetTokenRequest(_clientId, _clientSecret, username, password));

            if (tokenResponse.Code != ResponseCode.OK)
            {
                return new Response(tokenResponse.Code, tokenResponse.Message);
            }

            SaveGetTokenResponse(tokenResponse.Content.Value);

            return new Response(ResponseCode.OK, null);
        }

        public Response Logout()
        {
            if (string.IsNullOrWhiteSpace(_refreshToken))
            {
                return new Response(ResponseCode.NotSent, "User is not logged in");
            }

            var request = CreateAuthorizedRequest(_accessToken, $"api/refreshToken/{_refreshToken}");

            var response = Client.Delete(request);

            var output = Parse(response, HttpStatusCode.NoContent);

            if (output.Code == ResponseCode.OK)
            {
                SaveGetTokenResponse(new GetTokenResponse());
            }

            return output;
        }

        private Response<GetTokenResponse?> GetToken(GetTokenRequest getTokenRequest)
        {
            var request = new RestRequest(GetUrl("token"));

            request.AddParameter("client_id", getTokenRequest.ClientId);

            request.AddParameter("client_secret", getTokenRequest.ClientSecret);

            if (string.IsNullOrWhiteSpace(getTokenRequest.RefreshToken))
            {
                request.AddParameter("username", getTokenRequest.UserName);

                request.AddParameter("password", getTokenRequest.Password);

                request.AddParameter("grant_type", GrantType.Password);
            }
            else
            {
                request.AddParameter("refresh_token", getTokenRequest.RefreshToken);

                request.AddParameter("grant_type", GrantType.RefreshToken);
            }

            var response = Client.Post(request);

            return Parse<GetTokenResponse?>(response);
        }

        private void SaveGetTokenResponse(GetTokenResponse getTokenResponse)
        {
            _accessToken = getTokenResponse.AccessToken;

            _expirationDate = getTokenResponse.ExpirationDate;

            _refreshToken = getTokenResponse.RefreshToken;

            _userId = getTokenResponse.UserId;

            File.WriteAllText(".refresh", _refreshToken ?? string.Empty);
        }

        private struct GrantType
        {
            public static string Password = "password";

            public static string RefreshToken = "refresh_token";
        }

        private string _refreshToken;

        private string _accessToken;

        private int? _userId;

        private DateTimeOffset? _expirationDate;

        private readonly int _clientId;

        private readonly string _clientSecret;

        private readonly IUserClient _userClient;
    }
}