﻿using BlackPlain.Authorization.Web;
using BlackPlain.Core.Web;
using System.Collections.Generic;

namespace BlackPlain.Authorization.Client
{
    public interface IUserClient
    {
        Response<string> Ping(string message);

        Response<int?> Create(CreateUserRequest userDto);

        Response<User> Retrieve(string token, int userId);

        Response<IEnumerable<User>> Retrieve(string token);

        Response GeneratePasswordResetToken(GeneratePasswordResetTokenRequest generatePasswordResetTokenRequest);

        Response ResetPassword(ResetPasswordRequest resetPasswordRequest);
    }
}