﻿using BlackPlain.Core.Web;

namespace BlackPlain.Authorization.Client
{
    public interface IAuthorizationClient
    {
        IUserClient UserClient { get; }

        string AccessToken { get; }

        int? UserId { get; }

        Response Authorize();

        Response Login(string username, string password);

        Response Logout();
    }
}