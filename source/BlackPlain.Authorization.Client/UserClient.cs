﻿using BlackPlain.Authorization.Web;
using BlackPlain.Core.Web;
using RestSharp;
using System.Collections.Generic;
using System.Net;

namespace BlackPlain.Authorization.Client
{
    internal class UserClient : ClientBase, IUserClient
    {
        public UserClient(IRestClient client)
            : base(client, "api/user")
        {
        }

        public Response<string> Ping(string message)
        {
            var request = new RestRequest(GetUrl("ping/{0}", message));

            var response = Client.Get(request);

            return Parse<string>(response);
        }

        public Response<int?> Create(CreateUserRequest userDto)
        {
            var request = new RestRequest(GetUrl());

            request.AddJsonBody(userDto);

            var response = Client.Post(request);

            return Parse<int?>(response, HttpStatusCode.Created);
        }

        public Response<User> Retrieve(string token, int userId)
        {
            var request = CreateAuthorizedRequest(token, GetUrl("{0}", userId));

            var response = Client.Get(request);

            return Parse<User>(response);
        }

        public Response<IEnumerable<User>> Retrieve(string token)
        {
            var request = new RestRequest(GetUrl());

            var response = Client.Get(request);

            return Parse<IEnumerable<User>>(response);
        }

        public Response GeneratePasswordResetToken(GeneratePasswordResetTokenRequest generatePasswordResetTokenRequest)
        {
            var request = new RestRequest(GetUrl("passwordReset"));

            request.AddJsonBody(generatePasswordResetTokenRequest);

            var response = Client.Post(request);

            return Parse(response);
        }

        public Response ResetPassword(ResetPasswordRequest resetPasswordRequest)
        {
            var request = new RestRequest(GetUrl("passwordReset"));

            request.AddJsonBody(resetPasswordRequest);

            var response = Client.Put(request);

            return Parse(response);
        }

        public Response ChangePassword(string token, ChangePasswordRequest changePasswordRequest)
        {
            var request = CreateAuthorizedRequest(token, GetUrl("password"));

            request.AddJsonBody(changePasswordRequest);

            var response = Client.Put(request);

            return Parse(response);
        }
    }
}