﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("BlackPlain.Authorization.Client")]
[assembly: AssemblyDescription("BlackPlain authorization REST client")]
[assembly: Guid("23a3b05e-3086-4ac5-bc4e-e00e5c56d3b9")]
[assembly: AssemblyVersion("1.0.15")]
[assembly: AssemblyFileVersion("1.0.15")]

#if DEBUG
[assembly: AssemblyConfiguration("Debug")]
#else
[assembly: AssemblyConfiguration("Release")]
#endif
[assembly: AssemblyCompany("Black Plain Games")]
[assembly: AssemblyProduct("BlackPlain.Authorization.Client")]
[assembly: AssemblyCopyright("Copyright ©  2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]