﻿namespace BlackPlain.Authorization
{
    public static class Templates
    {
        public const string ConfirmationEmail = @"~\Templates\ConfirmEmail.template";

        public const string EmailVerificationSucceeded = @"~\Templates\EmailVerificationSucceeded.template";

        public const string EmailVerificationFailed = @"~\Templates\EmailVerificationFailed.template";

        public const string PasswordResetEmail = @"~\Templates\PasswordReset.template";
    }
}