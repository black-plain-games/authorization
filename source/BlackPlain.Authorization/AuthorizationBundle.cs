﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Providers;
using BlackPlain.DI;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;

namespace BlackPlain.Authorization
{
    public class AuthorizationBundle : IResolverBundle
    {
        public void CreateResolvers(IHub hub)
        {
            hub.CreateResolver<IOAuthAuthorizationServerProvider, SimpleAuthorizationServerProvider<Hub>>();

            hub.CreateResolver<IAuthenticationTokenProvider, SimpleRefreshTokenProvider<Hub>>();

            hub.CreateResolver<IUserTokenProvider<User, int>, SimpleUserTokenProvider<Hub>>();

            hub.CreateResolver<IOAuthBearerAuthenticationProvider, SimpleBearerAuthenticationProvider<Hub>>();
        }

        public void SetResolutions(IHub hub)
        {
        }
    }
}