﻿using System;
using System.Linq;

namespace BlackPlain.Authorization
{
    public static class Helpers
    {
        public static Core.User Convert(Web.CreateUserRequest createUserRequest)
        {
            if (createUserRequest == null)
            {
                return null;
            }

            return new Core.User
            {
                UserName = createUserRequest.UserName,
                EmailAddress = createUserRequest.EmailAddress,
                PhoneNumber = createUserRequest.PhoneNumber,
                FirstName = createUserRequest.FirstName,
                LastName = createUserRequest.LastName
            };
        }

        public static Web.User Convert(Core.User user)
        {
            if (user == null)
            {
                return null;
            }

            return new Web.User
            {
                Id = user.Id,
                DateCreated = user.DateCreated,
                DateModified = user.DateModified,
                UserName = user.UserName,
                EmailAddress = user.EmailAddress,
                IsEmailAddressVerified = user.IsEmailAddressVerified,
                PhoneNumber = user.PhoneNumber,
                IsPhoneNumberVerified = user.IsPhoneNumberVerified,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
        }

        public static Core.Client Convert(Web.CreateClientRequest createClientRequest)
        {
            if (createClientRequest == null)
            {
                return null;
            }

            return new Core.Client
            {
                Name = createClientRequest.Name,
                Secret = createClientRequest.Secret,
                RefreshTokenLifeTime = createClientRequest.RefreshTokenLifeTime,
                AllowedOrigin = createClientRequest.AllowedOrigin
            };
        }

        public static string GetActiveBanDescription(int clientId, Core.User user)
        {
            var activeBan = user.Bans.FirstOrDefault(b => IsActiveBan(b, clientId));

            if (activeBan != null)
            {
                if (activeBan.EndDate.HasValue)
                {
                    return $"User is banned until {activeBan.EndDate:g}";
                }
                else
                {
                    return "User is banned indefinitely";
                }
            }

            return null;
        }

        private static bool IsActiveBan(Core.Ban ban, int clientId)
        {
            if (ban.ClientId.HasValue &&
                ban.ClientId != clientId)
            {
                return false;
            }

            if (ban.StartDate > DateTimeOffset.Now)
            {
                return false;
            }

            if (ban.EndDate.HasValue &&
                ban.EndDate < DateTimeOffset.Now)
            {
                return false;
            }

            return true;
        }
    }
}