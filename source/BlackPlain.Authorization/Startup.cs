﻿using BlackPlain.Authorization.Repositories;
using BlackPlain.Authorization.Services;
using BlackPlain.DI;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security.Infrastructure;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Configuration;
using System.Web.Http;

[assembly: OwinStartup(typeof(BlackPlain.Authorization.Startup))]
namespace BlackPlain.Authorization
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            Hub.LogEverything = true;

            Hub.RegisterResolverBundleStatic<AuthorizationBundle>();

            Hub.RegisterResolverBundleStatic<RepositoriesBundle>();

            Hub.RegisterResolverBundleStatic<ServicesBundle>();

            var hub = new Hub();

            var accessTokenExpireMinutes = int.Parse(ConfigurationManager.AppSettings["AccessTokenExpireMinutes"]);

            app.UseOAuthAuthorizationServer(new OAuthAuthorizationServerOptions
            {
#if DEBUG
                AllowInsecureHttp = true,
#endif
                TokenEndpointPath = new PathString("/api/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(accessTokenExpireMinutes),
                Provider = hub.Resolve<IOAuthAuthorizationServerProvider>(),
                RefreshTokenProvider = hub.Resolve<IAuthenticationTokenProvider>()
            });

            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            var config = new HttpConfiguration();

            WebApiConfig.Register(config);

            app.UseCors(CorsOptions.AllowAll);

            app.UseWebApi(config);
        }
    }
}