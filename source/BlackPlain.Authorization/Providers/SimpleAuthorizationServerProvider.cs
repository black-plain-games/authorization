﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Services;
using BlackPlain.DI;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Providers
{
    public class SimpleAuthorizationServerProvider<THub> : OAuthAuthorizationServerProvider
        where THub : IHub, new()
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            var clientIdString = string.Empty;

            var clientSecret = string.Empty;

            if (!context.TryGetBasicCredentials(out clientIdString, out clientSecret))
            {
                context.TryGetFormCredentials(out clientIdString, out clientSecret);
            }

            if (context.ClientId == null ||
                !int.TryParse(context.ClientId, out var clientId))
            {
                context.SetError("invalid_clientId", "ClientId is required.");

                return;
            }

            var client = await ClientService.Retrieve(clientId);

            if (client == null)
            {
                context.SetError("invalid_clientId", $"Client '{clientId}' is not registered.");

                return;
            }

            if (string.IsNullOrWhiteSpace(clientSecret))
            {
                context.SetError("invalid_clientSecret", "Client secret is required.");

                return;
            }

            if (client.Secret != CryptoHelper.GetHash(clientSecret))
            {
                context.SetError("invalid_clientSecret", "Client secret is invalid.");

                return;
            }

            if (!client.IsActive)
            {
                context.SetError("invalid_clientId", "Client is inactive.");

                return;
            }

            context.OwinContext.Set("as:clientAllowedOrigin", client.AllowedOrigin);

            context.OwinContext.Set("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin") ?? string.Empty;

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var user = await UserService.Retrieve(context.UserName, context.Password);

            if (user == null)
            {
                context.SetError("invalid_grant", "The user name or password is incorrect.");

                return;
            }

            var clientId = int.Parse(context.ClientId);

            var activeBanDescription = Helpers.GetActiveBanDescription(clientId, user);

            if (!string.IsNullOrWhiteSpace(activeBanDescription))
            {
                context.SetError("user_banned", activeBanDescription);

                return;
            }

            var identity = new ClaimsIdentity(context.Options.AuthenticationType);

            identity.AddClaims(await GetClaims(user));

            var authenticationProperties = new AuthenticationProperties(new Dictionary<string, string>
            {
                { AuthenticationProperty.ClientId, context.ClientId },
                { AuthenticationProperty.UserId, $"{user.Id}" }
            });

            var ticket = new AuthenticationTicket(identity, authenticationProperties);

            context.Validated(ticket);
        }

        public override async Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            foreach (var property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }
        }

        private async Task<IEnumerable<Claim>> GetClaims(User user)
        {
            var output = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(ClaimTypes.NameIdentifier, $"{user.Id}")
            };

            foreach (var roleId in user.RoleIds)
            {
                var role = await RoleService.Retrieve(roleId);

                output.Add(new Claim(ClaimTypes.Role, role.Name));
            }

            return output;
        }

        public override async Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var originalClient = context.Ticket.Properties.Dictionary[AuthenticationProperty.ClientId];

            var currentClient = context.ClientId;

            if (originalClient != currentClient)
            {
                context.SetError("invalid_clientId", "Refresh token is issued to a different client.");
            }

            var idClaim = context.Ticket.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            var userId = int.Parse(idClaim.Value);

            var user = await UserService.Retrieve(userId);

            if (user == null)
            {
                context.SetError("invalid_grant", "Invalid user.");

                return;
            }

            var identity = new ClaimsIdentity(context.Ticket.Identity);

            identity.AddClaims(await GetClaims(user));

            var newTicket = new AuthenticationTicket(identity, context.Ticket.Properties);

            context.Validated(newTicket);
        }

        private IClientService ClientService => new THub().Resolve<IClientService>();

        private IUserService UserService => new THub().Resolve<IUserService>();

        private IRoleService RoleService => new THub().Resolve<IRoleService>();
    }
}