﻿using BlackPlain.Authorization.Core.Services;
using BlackPlain.DI;
using Microsoft.Owin.Security.OAuth;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Providers
{
    public class SimpleBearerAuthenticationProvider<THub> : IOAuthBearerAuthenticationProvider
        where THub : IHub, new()
    {
        public async Task ApplyChallenge(OAuthChallengeContext context)
        {
        }

        public async Task RequestToken(OAuthRequestTokenContext context)
        {
        }

        public async Task ValidateIdentity(OAuthValidateIdentityContext context)
        {
            var clientId = int.Parse(context.Ticket.Properties.Dictionary[AuthenticationProperty.ClientId]);

            var userId = int.Parse(context.Ticket.Properties.Dictionary[AuthenticationProperty.UserId]);

            var user = await UserService.Retrieve(userId);

            var activeBanDescription = Helpers.GetActiveBanDescription(clientId, user);

            if (!string.IsNullOrWhiteSpace(activeBanDescription))
            {
                context.SetError("user_banned", activeBanDescription);

                return;
            }
        }

        private IUserService UserService => new THub().Resolve<IUserService>();
    }
}