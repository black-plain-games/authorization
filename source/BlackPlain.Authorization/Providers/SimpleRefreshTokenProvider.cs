﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Services;
using BlackPlain.DI;
using Microsoft.Owin.Security.Infrastructure;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Providers
{
    public class SimpleRefreshTokenProvider<THub> : IAuthenticationTokenProvider
        where THub : IHub, new()
    {
        public void Create(AuthenticationTokenCreateContext context)
        {
            CreateAsync(context).Wait();
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var clientIdString = context.Ticket.Properties.Dictionary["client_id"];

            if (string.IsNullOrEmpty(clientIdString) ||
                !int.TryParse(clientIdString, out var clientId))
            {
                return;
            }

            var nameIdentifierClaim = context.Ticket.Identity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

            if (nameIdentifierClaim == null ||
                !int.TryParse(nameIdentifierClaim.Value, out var userId))
            {
                return;
            }

            var refreshTokenId = Guid.NewGuid().ToString("N");

            var refreshTokenLifeTimeString = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");

            if (!TimeSpan.TryParse(refreshTokenLifeTimeString, out var refreshTokenLifeTime))
            {
                refreshTokenLifeTime = TimeSpan.FromHours(1);
            }

            var token = new RefreshToken
            {
                Id = CryptoHelper.GetHash(refreshTokenId),
                UserId = userId,
                ClientId = clientId,
                IssueDate = DateTimeOffset.UtcNow,
                ExpirationDate = DateTimeOffset.UtcNow.Add(refreshTokenLifeTime)
            };

            context.Ticket.Properties.IssuedUtc = token.IssueDate;

            context.Ticket.Properties.ExpiresUtc = token.ExpirationDate;

            token.ProtectedTicket = context.SerializeTicket();

            if (await RefreshTokenService.Create(token))
            {
                context.SetToken(refreshTokenId);
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            ReceiveAsync(context).Wait();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

            context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

            var hashedTokenId = CryptoHelper.GetHash(context.Token);

            var refreshToken = await RefreshTokenService.Retrieve(hashedTokenId);

            if (refreshToken != null)
            {
                context.DeserializeTicket(refreshToken);
            }
        }

        private IRefreshTokenService RefreshTokenService => new THub().Resolve<IRefreshTokenService>();
    }
}