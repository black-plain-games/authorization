﻿using BlackPlain.Authorization.Core;
using BlackPlain.Authorization.Core.Services;
using BlackPlain.DI;
using Microsoft.AspNet.Identity;
using System;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Providers
{
    public class SimpleUserTokenProvider<THub> : IUserTokenProvider<User, int>
        where THub : IHub, new()
    {
        public async Task<string> GenerateAsync(string purpose, UserManager<User, int> manager, User user)
        {
            return await UserTokenService.Create(user.Id, Convert(purpose));
        }

        public async Task<bool> IsValidProviderForUserAsync(UserManager<User, int> manager, User user)
        {
            return user != null;
        }

        public Task NotifyAsync(string token, UserManager<User, int> manager, User user)
        {
            throw new NotImplementedException();
        }

        public async Task<bool> ValidateAsync(string purpose, string token, UserManager<User, int> manager, User user)
        {
            return await UserTokenService.Validate(user.Id, Convert(purpose), token);
        }

        private static TokenPurpose Convert(string purpose)
        {
            switch (purpose.ToUpper())
            {
                case "CONFIRMATION":
                    return TokenPurpose.Confirmation;

                case "RESETPASSWORD":
                    return TokenPurpose.ResetPassword;

                default:
                    throw new ArgumentOutOfRangeException(nameof(purpose), purpose, "Token purpose not supported");
            }
        }

        private IUserTokenService UserTokenService => new THub().Resolve<IUserTokenService>();
    }
}