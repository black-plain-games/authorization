﻿namespace BlackPlain.Authorization.Providers
{
    public static class AuthenticationProperty
    {
        public const string ClientId = "client_id";

        public const string UserId = "user_id";
    }
}