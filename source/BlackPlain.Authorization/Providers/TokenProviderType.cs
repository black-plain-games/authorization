﻿namespace BlackPlain.Authorization.Providers
{
    public enum TokenProviderType : byte
    {
        AccessToken,
        RefreshToken
    }
}