﻿using BlackPlain.Authorization.Core.Services;
using BlackPlain.DI;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Web;
using System.Web.Http;

namespace BlackPlain.Authorization.Controllers
{
    public class BaseController<THub> : ApiController
        where THub : IHub, new()
    {
        protected IRefreshTokenService RefreshTokenService => _services.Resolve<IRefreshTokenService>();

        protected IUserService UserService => _services.Resolve<IUserService>();

        protected IEmailService EmailService => _services.Resolve<IEmailService>();

        protected IRoleService RoleService => _services.Resolve<IRoleService>();

        protected IClientService ClientService => _services.Resolve<IClientService>();

        protected BaseController()
        {
            _services = new Hub();
        }

        protected IHttpActionResult Response(HttpStatusCode code)
        {
            return Response(code, null);
        }

        protected IHttpActionResult Response(HttpStatusCode code, object content)
        {
            var response = new HttpResponseMessage(code);

            if (content != null)
            {
                response.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
            };

            return ResponseMessage(response);
        }

        protected static string GetTemplate(string relativeFilePath, params object[] args)
        {
            if (!_templates.ContainsKey(relativeFilePath))
            {
                var currentWorkingDirectory = HttpContext.Current.Request.MapPath(relativeFilePath);

                _templates[relativeFilePath] = File.ReadAllText(currentWorkingDirectory);
            }

            return string.Format(_templates[relativeFilePath], args);
        }

        protected int? UserId
        {
            get
            {
                var principal = (ClaimsPrincipal)RequestContext.Principal;

                if (principal == null)
                {
                    return null;
                }

                var userIdClaim = principal.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier);

                if (userIdClaim == null ||
                    !int.TryParse(userIdClaim.Value, out int userId))
                {
                    return null;
                }

                return userId;
            }
        }

        private static IDictionary<string, string> _templates = new Dictionary<string, string>();

        private readonly IHub _services = new THub();
    }
}