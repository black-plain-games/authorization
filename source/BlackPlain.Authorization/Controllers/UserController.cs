﻿using BlackPlain.Authorization.Web;
using BlackPlain.DI;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackPlain.Authorization.Controllers
{
    [RoutePrefix("api/user")]
    public class UserController : BaseController<Hub>
    {
        public UserController()
            : base()
        {
        }

        [AllowAnonymous]
        [HttpGet, Route("ping/{message}")]
        public async Task<IHttpActionResult> Ping(string message)
        {
            return Ok($"Server heard '{message}'");
        }

        [AllowAnonymous]
        [HttpGet, Route("verifyEmail")]
        public async Task<HttpResponseMessage> VerifyEmail([FromUri]int userId, [FromUri]string token)
        {
            var result = await UserService.ConfirmEmail(userId, token);

            string response;

            if (result.IsSuccess)
            {
                response = GetTemplate(Templates.EmailVerificationSucceeded);
            }
            else
            {
                response = GetTemplate(Templates.EmailVerificationFailed, string.Join(string.Empty, result.Messages.Select(m => $"<div>{m}</div>")));
            }

            var content = new StringContent(response);

            content.Headers.ContentType = new MediaTypeHeaderValue("text/html");

            return new HttpResponseMessage
            {
                Content = content
            };
        }

        [AllowAnonymous]
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Create(CreateUserRequest createUserRequest)
        {
            return await Create(createUserRequest, "User");
        }

        [AllowAnonymous]
        [HttpPost, Route("passwordReset")]
        public async Task<IHttpActionResult> GeneratePasswordResetToken(GeneratePasswordResetTokenRequest generatePasswordResetTokenRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserService.RetrieveByUserName(generatePasswordResetTokenRequest.UserName);

            if (user == null)
            {
                user = await UserService.RetrieveByEmailAddress(generatePasswordResetTokenRequest.EmailAddress);
            }

            if (string.IsNullOrWhiteSpace(user.EmailAddress))
            {
                return BadRequest(_contactSupportMessage);
            }

            if (!user.IsEmailAddressVerified.HasValue ||
                !user.IsEmailAddressVerified.Value)
            {
                return BadRequest(_contactSupportMessage);
            }

            if (string.Compare(user.EmailAddress, generatePasswordResetTokenRequest.EmailAddress, true) != 0)
            {
                return BadRequest(_contactSupportMessage);
            }

            var token = await UserService.GeneratePasswordResetToken(user.Id);

            var passwordResetTemplate = GetTemplate(Templates.PasswordResetEmail, token);

            EmailService.SendEmail("BlackPlain Account Password Reset", passwordResetTemplate, user.EmailAddress);

            return Ok();
        }

        [AllowAnonymous]
        [HttpPut, Route("passwordReset")]
        public async Task<IHttpActionResult> ResetPassword([FromBody]ResetPasswordRequest resetPasswordRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await UserService.RetrieveByUserName(resetPasswordRequest.UserName);

            if (user == null)
            {
                return NotFound();
            }

            var result = await UserService.ResetPassword(user.Id, resetPasswordRequest.Token, resetPasswordRequest.Password);

            if (!result.IsSuccess)
            {
                var messages = string.Join(", ", result.Messages);

                return InternalServerError(new Exception($"Password reset failed. {messages}"));
            }

            return Ok();
        }

        [Authorize(Roles = "User,Administrator")]
        [HttpPut, Route("{password}")]
        public async Task<IHttpActionResult> ChangePassword([FromBody]ChangePasswordRequest changePasswordRequest)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var result = await UserService.ChangePassword(UserId.Value, changePasswordRequest.CurrentPassword, changePasswordRequest.NewPassword);

            if (!result.IsSuccess)
            {
                var messages = string.Join(", ", result.Messages);

                return InternalServerError(new Exception($"Change password failed. {messages}"));
            }

            return Ok();
        }

        [Authorize(Roles = "User,Administrator")]
        [HttpGet, Route("{userId}")]
        public async Task<IHttpActionResult> Retrieve(int userId)
        {
            if (userId <= 0)
            {
                return BadRequest("Invalid user id");
            }

            if (!User.IsInRole("Administrator") && userId != UserId)
            {
                return BadRequest("Cannot update other users");
            }

            var user = await UserService.Retrieve(userId);

            if (user == null)
            {
                return NotFound();
            }

            var userDto = Helpers.Convert(user);

            return Ok(userDto);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost, Route("administrator")]
        public async Task<IHttpActionResult> CreateAdministrator(CreateUserRequest createUserRequest)
        {
            return await Create(createUserRequest, "Administrator");
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet, Route("")]
        public async Task<IHttpActionResult> Retrieve()
        {
            return Ok(UserService.Retrieve().Select(u => Helpers.Convert(u)));
        }

        private async Task<IHttpActionResult> Create(CreateUserRequest createUserRequest, string role)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = Helpers.Convert(createUserRequest);

            var userId = await UserService.Create(user, createUserRequest.Password);

            if (!userId.HasValue)
            {
                return Response(HttpStatusCode.InternalServerError, $"Failed to create user");
            }

            if (!await UserService.AddToRole(userId.Value, role))
            {
                return Response(HttpStatusCode.InternalServerError, $"Failed to assign role '{role}' to user '{userId}'");
            }

            var token = await UserService.GenerateEmailConfirmationToken(userId.Value);

            var confirmationEmailTemplate = GetTemplate(Templates.ConfirmationEmail, Request.RequestUri, userId.Value, token);

            EmailService.SendEmail("BlackPlain Account Email Confirmation", confirmationEmailTemplate, user.EmailAddress);

            return Created($"{Request.RequestUri}/{userId}", userId.Value);
        }

        private static readonly string _contactSupportMessage = "Please contact support@BlackPlainstudios.com for assistance.";
    }
}