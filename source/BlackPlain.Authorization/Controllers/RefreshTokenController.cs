﻿using BlackPlain.Authorization.Core;
using BlackPlain.DI;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackPlain.Authorization.Controllers
{
    [RoutePrefix("api/refreshToken")]
    public class RefreshTokenController : BaseController<Hub>
    {
        public RefreshTokenController()
            : base()
        {
        }

        [HttpDelete]
        [Route("{refreshToken}")]
        public async Task<IHttpActionResult> Delete(string refreshToken)
        {
            var tokenId = CryptoHelper.GetHash(refreshToken);

            if (await RefreshTokenService.Delete(tokenId))
            {
                return Response(HttpStatusCode.NoContent);
            }

            return BadRequest("Refresh token does not exist");
        }
    }
}