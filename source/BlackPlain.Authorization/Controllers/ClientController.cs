﻿using BlackPlain.Authorization.Web;
using BlackPlain.DI;
using System.Threading.Tasks;
using System.Web.Http;

namespace BlackPlain.Authorization.Controllers
{
    [RoutePrefix("api/client")]
    public class ClientController : BaseController<Hub>
    {
        public ClientController()
            : base()
        {
        }

        [AllowAnonymous]
        [HttpGet, Route("ping/{message}")]
        public async Task<IHttpActionResult> Ping(string message)
        {
            return Ok($"Server heard '{message}'");
        }

        [Authorize(Roles = "Administrator")]
        [HttpPost, Route("")]
        public async Task<IHttpActionResult> Create([FromBody]CreateClientRequest createClientRequest)
        {
            var client = Helpers.Convert(createClientRequest);

            var clientId = await ClientService.Create(client);

            return Created("", clientId);
        }

        [Authorize(Roles = "Administrator")]
        [HttpGet, Route("{clientId}")]
        public async Task<IHttpActionResult> Retrieve(int clientId)
        {
            var client = await ClientService.Retrieve(clientId);

            if (client == null)
            {
                return NotFound();
            }

            return Ok(client);
        }

        [Authorize(Roles = "Administrator")]
        [HttpPut, Route("{clientId}")]
        public async Task<IHttpActionResult> Update(int clientId, [FromBody]UpdateClientRequest updateClientRequest)
        {
            var client = await ClientService.Retrieve(clientId);

            if (client == null)
            {
                return NotFound();
            }

            client.AllowedOrigin = updateClientRequest.AllowedOrigin;

            client.IsActive = updateClientRequest.IsActive;

            client.Name = updateClientRequest.Name;

            client.RefreshTokenLifeTime = updateClientRequest.RefreshTokenLifeTime;

            client.Secret = updateClientRequest.Secret;

            await ClientService.Update(client);

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost, Route("{clientId}/secret")]
        public async Task<IHttpActionResult> Update(int clientId)
        {
            var client = await ClientService.Retrieve(clientId);

            if (client == null)
            {
                return NotFound();
            }

            await ClientService.Update(client);

            return Ok();
        }
    }
}