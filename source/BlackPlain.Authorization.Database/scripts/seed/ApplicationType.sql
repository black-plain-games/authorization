﻿SET IDENTITY_INSERT [dbo].[ApplicationType] ON

MERGE INTO [dbo].[ApplicationType] AS TARGET
    USING ( VALUES
        (1, 'Native'),
        (2, 'Web')
    ) AS SOURCE ([Id], [Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name])
    VALUES ([Id], [Name])
;

SET IDENTITY_INSERT [dbo].[ApplicationType] OFF