﻿SET IDENTITY_INSERT [dbo].[TokenPurpose] ON

MERGE INTO [dbo].[TokenPurpose] AS TARGET
    USING ( VALUES
        (1, 'Confirmation'),
        (2, 'Reset Password')
    ) AS SOURCE ([Id], [Name])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name]
WHEN NOT MATCHED BY TARGET THEN
    INSERT ([Id], [Name])
    VALUES ([Id], [Name])
;


SET IDENTITY_INSERT [dbo].[TokenPurpose] OFF