﻿SET IDENTITY_INSERT [dbo].[Client] ON

MERGE INTO [dbo].[Client] AS TARGET
    USING ( VALUES
        (1, 'BlackPlain', 1, 1, 'BlackPlain-secret', 60, '*'),
        (2, 'Bizio', 1, 1, 'bizio-secret', 60, '*')
    ) AS SOURCE (
        [Id],
        [Name],
        [ApplicationTypeId],
        [IsActive],
        [Secret],
        [RefreshTokenLifeTimeMinutes],
        [AllowedOrigin])
ON TARGET.[Id] = SOURCE.[Id]
WHEN MATCHED THEN
    UPDATE SET
        [Name] = SOURCE.[Name],
        [ApplicationTypeId] = SOURCE.[ApplicationTypeId],
        [IsActive] = SOURCE.[IsActive],
        [RefreshTokenLifeTimeMinutes] = SOURCE.[RefreshTokenLifeTimeMinutes],
        [AllowedOrigin] = SOURCE.[AllowedOrigin]
WHEN NOT MATCHED BY TARGET THEN
    INSERT (
        [Id],
        [Name],
        [ApplicationTypeId],
        [IsActive],
        [Secret],
        [RefreshTokenLifeTimeMinutes],
        [AllowedOrigin])
    VALUES (
        [Id],
        [Name],
        [ApplicationTypeId],
        [IsActive],
        [Secret],
        [RefreshTokenLifeTimeMinutes],
        [AllowedOrigin])
;

SET IDENTITY_INSERT [dbo].[Client] OFF