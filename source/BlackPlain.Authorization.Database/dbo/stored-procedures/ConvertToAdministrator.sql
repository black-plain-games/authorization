﻿CREATE PROCEDURE [dbo].[ConvertToAdministrator]
    @UserId INT
AS
    DECLARE @NewUserId INT = @UserId - 9999

    SET IDENTITY_INSERT [User] ON

    INSERT INTO [User] (Id, DateCreated, DateModified, UserName, PasswordHash, EmailAddress, IsEmailAddressVerified, PhoneNumber, IsPhoneNumberVerified, FirstName, LastName)
    SELECT TOP 1 @NewUserId AS Id, DateCreated, DateModified, UserName, PasswordHash, EmailAddress, IsEmailAddressVerified, PhoneNumber, IsPhoneNumberVerified, FirstName, LastName
    FROM [User]
    WHERE Id = @userId

    SET IDENTITY_INSERT [User] OFF

    DELETE FROM [UserToken]
    WHERE UserId = @userId

    DELETE FROM [UserRole]
    WHERE UserId = @userId

    DELETE FROM [User]
    WHERE Id = @userId

    INSERT INTO [UserRole] VALUES
    (@NewUserId, 1),
    (@NewUserId, 2)

    RETURN @NewUserId