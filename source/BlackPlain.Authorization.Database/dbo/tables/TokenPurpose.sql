﻿CREATE TABLE [dbo].[TokenPurpose]
(
    [Id] TINYINT NOT NULL IDENTITY,
    [Name] NVARCHAR(128) NOT NULL,
    CONSTRAINT [PK_TokenPurpose]
        PRIMARY KEY CLUSTERED ([Id])
)