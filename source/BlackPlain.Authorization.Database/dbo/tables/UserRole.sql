﻿CREATE TABLE [dbo].[UserRole]
(
    [UserId] INT NOT NULL,
    [RoleId] TINYINT NOT NULL,
    CONSTRAINT [PK_UserRole]
        PRIMARY KEY CLUSTERED ([UserId], [RoleId]),
    CONSTRAINT [FK_UserRole_UserId]
        FOREIGN KEY ([UserId])
        REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_UserRole_RoleId]
        FOREIGN KEY ([RoleId])
        REFERENCES [dbo].[Role] ([Id])
)

GO;