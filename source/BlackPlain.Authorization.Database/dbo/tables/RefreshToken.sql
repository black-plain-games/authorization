﻿CREATE TABLE [dbo].[RefreshToken]
(
    [Id] NVARCHAR(128) NOT NULL,
    [UserId] INT NOT NULL,
    [ClientId] INT NOT NULL,
    [IssueDate] DATETIMEOFFSET NOT NULL,
    [ExpirationDate] DATETIMEOFFSET NOT NULL,
    [ProtectedTicket] NVARCHAR(MAX) NOT NULL,
    CONSTRAINT [PK_RefreshToken]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_RefreshToken_UserId]
        FOREIGN KEY ([UserId])
        REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_RefreshToken_ClientId]
        FOREIGN KEY ([ClientId])
        REFERENCES [dbo].[Client] ([Id])
)

GO;