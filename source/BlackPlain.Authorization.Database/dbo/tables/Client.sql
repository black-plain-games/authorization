﻿CREATE TABLE [dbo].[Client]
(
    [Id] INT NOT NULL IDENTITY(10000, 1),
    [DateCreated] DATETIMEOFFSET NOT NULL,
    [ApplicationTypeId] TINYINT NOT NULL,
    [Name] NVARCHAR(128) NOT NULL,
    [IsActive] BIT NOT NULL,
    [Secret] NVARCHAR(MAX) NOT NULL,
    [RefreshTokenLifeTimeMinutes] INT NOT NULL,
    [AllowedOrigin] NVARCHAR(128) NOT NULL,
    CONSTRAINT [PK_Client]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_Client_ApplicationTypeId]
        FOREIGN KEY ([ApplicationTypeId])
        REFERENCES [dbo].[ApplicationType] ([Id])
)

GO;

ALTER TABLE [dbo].[Client]
    ADD CONSTRAINT [DF_Client_DateCreated]
    DEFAULT (GETDATE()) FOR [DateCreated]

GO;