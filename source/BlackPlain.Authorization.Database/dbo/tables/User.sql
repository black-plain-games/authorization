﻿CREATE TABLE [dbo].[User]
(
    [Id] INT NOT NULL IDENTITY(10000, 1),
    [DateCreated] DATETIMEOFFSET NOT NULL,
    [DateModified] DATETIMEOFFSET NOT NULL,
    [UserName] NVARCHAR(128) NOT NULL,
    [PasswordHash] NVARCHAR(128) NULL,
    [EmailAddress] NVARCHAR(128) NULL,
    [IsEmailAddressVerified] BIT NULL,
    [PhoneNumber] VARCHAR(15) NULL,
    [IsPhoneNumberVerified] BIT NULL,
    [FirstName] NVARCHAR(128) NOT NULL,
    [LastName] NVARCHAR(128) NOT NULL,
    CONSTRAINT [PK_User]
        PRIMARY KEY CLUSTERED ([Id])
)

GO;

ALTER TABLE [dbo].[User]
    ADD CONSTRAINT [DF_User_DateCreated]
    DEFAULT (GETDATE()) FOR [DateCreated]

GO;

ALTER TABLE [dbo].[User]
    ADD CONSTRAINT [DF_User_DateModified]
    DEFAULT (GETDATE()) FOR [DateModified]

GO;