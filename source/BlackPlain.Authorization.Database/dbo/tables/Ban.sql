﻿CREATE TABLE [dbo].[Ban]
(
    [Id] INT NOT NULL IDENTITY,
    [ClientId] INT NULL,
    [UserId] INT NOT NULL,
    [DateCreated] DATETIMEOFFSET NOT NULL,
    [StartDate] DATETIMEOFFSET NOT NULL,
    [EndDate] DATETIMEOFFSET NULL,
    [Details] NVARCHAR(1024) NOT NULL,
    CONSTRAINT [PK_Ban]
        PRIMARY KEY CLUSTERED ([Id]),
    CONSTRAINT [FK_Ban_ClientId]
        FOREIGN KEY ([ClientId])
        REFERENCES [dbo].[Client] ([Id]),
    CONSTRAINT [FK_Ban_UserId]
        FOREIGN KEY ([UserId])
        REFERENCES [dbo].[User] ([Id])
)

GO;

ALTER TABLE [dbo].[Ban]
    ADD CONSTRAINT [DF_Ban_DateCreated]
    DEFAULT (GETDATE()) FOR [DateCreated]

GO;