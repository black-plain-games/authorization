﻿CREATE TABLE [dbo].[ApplicationType]
(
    [Id] TINYINT NOT NULL IDENTITY,
    [Name] NVARCHAR(128) NOT NULL,
    CONSTRAINT [PK_ApplicationType]
        PRIMARY KEY CLUSTERED ([Id])
)