﻿CREATE TABLE [dbo].[UserToken]
(
    [UserId] INT NOT NULL,
    [TokenPurposeId] TINYINT NOT NULL,
    [DateCreated] DATETIMEOFFSET NOT NULL,
    [DateModified] DATETIMEOFFSET NOT NULL,
    [IsValid] BIT NOT NULL,
    [Token] NVARCHAR(128) NOT NULL,
    CONSTRAINT [PK_UserToken]
        PRIMARY KEY CLUSTERED ([UserId], [TokenPurposeId]),
    CONSTRAINT [FK_UserToken_UserId]
        FOREIGN KEY ([UserId])
        REFERENCES [dbo].[User] ([Id]),
    CONSTRAINT [FK_UserToken_TokenPurposeId]
        FOREIGN KEY ([TokenPurposeId])
        REFERENCES [dbo].[TokenPurpose] ([Id])
)

GO;

ALTER TABLE [dbo].[UserToken]
    ADD CONSTRAINT [DF_UserToken_DateCreated]
    DEFAULT (GETDATE()) FOR [DateCreated]

GO;

ALTER TABLE [dbo].[UserToken]
    ADD CONSTRAINT [DF_UserToken_DateModified]
    DEFAULT (GETDATE()) FOR [DateModified]

GO;

ALTER TABLE [dbo].[UserToken]
    ADD CONSTRAINT [DF_UserToken_IsValid]
    DEFAULT (0) FOR [IsValid]

GO;