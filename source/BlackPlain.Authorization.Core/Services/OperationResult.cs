﻿using BlackPlain.Authorization.Core.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace BlackPlain.Authorization.Core.Services
{
    public struct OperationResult
    {
        public bool IsSuccess;

        public IEnumerable<string> Messages;

        public OperationResult(bool isSuccess, IEnumerable<string> messages)
        {
            IsSuccess = isSuccess;

            Messages = messages ?? Enumerable.Empty<string>();
        }

        public OperationResult(DbResult dbResult)
            : this(dbResult.IsSuccess, dbResult.Messages)
        {
        }
    }
}