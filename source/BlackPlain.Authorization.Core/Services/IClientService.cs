﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Services
{
    public interface IClientService
    {
        Task<int> Create(Client client);

        Task<Client> Retrieve(int clientId);

        Task Update(Client client);
    }
}