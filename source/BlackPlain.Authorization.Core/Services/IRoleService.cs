﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Services
{
    public interface IRoleService
    {
        Task<Role> Retrieve(byte roleId);
    }
}