﻿namespace BlackPlain.Authorization.Core.Services
{
    public interface IEmailService
    {
        void SendEmail(string subject, string body, string toEmailAddress);
    }
}