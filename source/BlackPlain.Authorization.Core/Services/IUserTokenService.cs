﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Services
{
    public interface IUserTokenService
    {
        Task<string> Create(int userId, TokenPurpose purpose);

        Task<bool> Validate(int userId, TokenPurpose purpose, string token);
    }
}