﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Services
{
    public interface IUserService
    {
        Task<int?> Create(User user, string password);

        Task<string> GenerateEmailConfirmationToken(int userId);

        Task<string> GeneratePasswordResetToken(int userId);

        Task<OperationResult> ConfirmEmail(int userId, string token);

        Task<OperationResult> ResetPassword(int userId, string token, string password);

        Task<OperationResult> ChangePassword(int userId, string currentPassword, string newPassword);

        IEnumerable<User> Retrieve();

        Task<User> Retrieve(int userId);

        Task<User> Retrieve(string userName, string password);

        Task<User> RetrieveByUserName(string userName);

        Task<User> RetrieveByEmailAddress(string emailAddress);

        Task<bool> AddToRole(int userId, string role);
    }
}