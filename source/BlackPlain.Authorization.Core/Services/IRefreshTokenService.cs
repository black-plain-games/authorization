﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Services
{
    public interface IRefreshTokenService
    {
        Task<bool> Create(RefreshToken refreshToken);

        Task<string> Retrieve(string refreshTokenId);

        Task<bool> Delete(string refreshTokenId);
    }
}