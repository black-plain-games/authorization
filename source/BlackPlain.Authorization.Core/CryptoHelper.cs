﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace BlackPlain.Authorization.Core
{
    public static class CryptoHelper
    {
        public static string GetHash(string input)
        {
            var algorithm = new SHA256CryptoServiceProvider();

            var value = Encoding.UTF8.GetBytes(input);

            var hash = algorithm.ComputeHash(value);

            return Convert.ToBase64String(hash);
        }
    }
}