﻿using System;

namespace BlackPlain.Authorization.Core
{
    public class RefreshToken
    {
        public string Id { get; set; }

        public int UserId { get; set; }

        public int ClientId { get; set; }

        public DateTimeOffset IssueDate { get; set; }

        public DateTimeOffset ExpirationDate { get; set; }

        public string ProtectedTicket { get; set; }
    }
}