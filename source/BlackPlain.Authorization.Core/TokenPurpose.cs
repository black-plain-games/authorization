﻿namespace BlackPlain.Authorization.Core
{
    public enum TokenPurpose : byte
    {
        Confirmation = 1,
        ResetPassword = 2
    }
}