﻿using Microsoft.AspNet.Identity;

namespace BlackPlain.Authorization.Core
{
    public class Role : IRole<byte>
    {
        public byte Id { get; set; }

        public string Name { get; set; }
    }
}