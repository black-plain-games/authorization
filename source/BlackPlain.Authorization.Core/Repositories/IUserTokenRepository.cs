﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IUserTokenRepository
    {
        Task<string> Create(int userId, TokenPurpose purpose);

        Task<bool> Validate(int userId, TokenPurpose purpose, string token);
    }
}