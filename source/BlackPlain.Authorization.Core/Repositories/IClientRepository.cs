﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IClientRepository
    {
        Task<int> Create(Client client);

        Task<Client> Retrieve(int clientId);

        Task Update(Client client);
    }
}