﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IRefreshTokenRepository
    {
        Task<DbResult> Create(RefreshToken token);

        Task<string> Retrieve(string refreshTokenId);

        Task<DbResult> Delete(string refreshTokenId);
    }
}