﻿using System.Collections.Generic;
using System.Linq;

namespace BlackPlain.Authorization.Core.Repositories
{
    public struct DbResult
    {
        public bool IsSuccess;

        public IEnumerable<string> Messages;

        public DbResult(bool isSuccess)
            : this(isSuccess, null)
        {
        }

        public DbResult(bool isSuccess, IEnumerable<string> messages)
        {
            IsSuccess = isSuccess;

            Messages = messages ?? Enumerable.Empty<string>();
        }

        public DbResult(bool isSuccess, params string[] messages)
        {
            IsSuccess = isSuccess;

            Messages = messages ?? Enumerable.Empty<string>();
        }
    }
}