﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IRoleManager
    {
        Task<Role> FindByIdAsync(byte roleId);
    }
}