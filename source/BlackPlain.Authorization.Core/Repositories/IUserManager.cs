﻿using Microsoft.AspNet.Identity;
using System.Linq;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IUserManager
    {
        IQueryable<User> Users { get; }

        Task<IdentityResult> CreateAsync(User user, string password);

        Task<User> FindAsync(string userName, string password);

        Task<User> FindByIdAsync(int userId);

        Task<User> FindByNameAsync(string userName);

        Task<User> FindByEmailAsync(string emailAddress);

        Task<string> GenerateEmailConfirmationTokenAsync(int userId);

        Task<string> GeneratePasswordResetTokenAsync(int userId);

        Task<IdentityResult> ConfirmEmailAsync(int userId, string token);

        Task<IdentityResult> ResetPasswordAsync(int userId, string token, string password);

        Task<IdentityResult> ChangePasswordAsync(int userId, string currentPassword, string newPassword);

        Task<IdentityResult> AddToRoleAsync(int userId, string role);
    }
}