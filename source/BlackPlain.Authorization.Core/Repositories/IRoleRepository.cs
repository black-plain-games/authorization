﻿using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IRoleRepository
    {
        Task<Role> Retrieve(byte roleId);
    }
}