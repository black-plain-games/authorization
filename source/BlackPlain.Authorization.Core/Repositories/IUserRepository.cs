﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace BlackPlain.Authorization.Core.Repositories
{
    public interface IUserRepository
    {
        Task<DbResult> Create(User user, string password);

        IEnumerable<User> Retrieve();

        Task<User> Retrieve(int userId);

        Task<User> Retrieve(string userName, string password);

        Task<User> RetrieveByUserName(string userName);

        Task<User> RetrieveByEmailAddress(string emailAddress);

        Task<string> GenerateEmailConfirmationToken(int userId);

        Task<string> GeneratePasswordResetToken(int userId);

        Task<DbResult> ConfirmEmail(int userId, string token);

        Task<DbResult> ResetPassword(int userId, string token, string password);

        Task<DbResult> ChangePassword(int userId, string currentPassword, string newPassword);

        Task<DbResult> AddToRole(int userId, string role);
    }
}