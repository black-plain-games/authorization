﻿using System;

namespace BlackPlain.Authorization.Core
{
    public class Ban
    {
        public int Id { get; set; }

        public int? ClientId { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public DateTimeOffset StartDate { get; set; }

        public DateTimeOffset? EndDate { get; set; }

        public string Details { get; set; }
    }
}