﻿using System;

namespace BlackPlain.Authorization.Core
{
    public class Client
    {
        public int Id { get; }

        public DateTimeOffset DateCreated { get; }

        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Secret { get; set; }

        public TimeSpan RefreshTokenLifeTime { get; set; }

        public string AllowedOrigin { get; set; }

        public Client(int id, DateTimeOffset dateCreated)
        {
            Id = id;

            DateCreated = dateCreated;
        }

        public Client()
        {
        }
    }
}