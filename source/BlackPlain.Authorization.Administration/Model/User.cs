﻿using BlackPlain.Wpf.Core;
using System;
using System.Collections.Generic;

namespace BlackPlain.Authorization.Administration.Model
{
    public class User : NotifyOnPropertyChanged
    {
        public string Id
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string UserName
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Password
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string EmailAddress
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool EmailConfirmed
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public string PhoneNumber
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool PhoneNumberConfirmed
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool LockoutEnabled
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public DateTimeOffset? LockoutEndDate
        {
            get
            {
                return GetField<DateTimeOffset?>();
            }
            set
            {
                SetField(value);
            }
        }

        public string SecurityStamp
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public bool TwoFactorEnabled
        {
            get
            {
                return GetField<bool>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<string> RoleIds
        {
            get
            {
                return GetField<IEnumerable<string>>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}