﻿using BlackPlain.Wpf.Core;

namespace BlackPlain.Authorization.Administration.Model
{
    public class Role : NotifyOnPropertyChanged
    {
        public string Id
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Name
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }
    }
}