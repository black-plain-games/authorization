﻿using BlackPlain.Authorization.Administration.Model;
using BlackPlain.Authorization.Models;
using System.Collections.Generic;
using System.Windows.Input;

namespace BlackPlain.Authorization.Administration.ViewModels
{
    public class UsersViewModel : AuthorizedViewModelBase
    {
        public IEnumerable<User> Users
        {
            get
            {
                return GetField<IEnumerable<User>>();
            }
            set
            {
                SetField(value);
            }
        }

        public User SelectedUser
        {
            get
            {
                return GetField<User>();
            }
            set
            {
                SetField(value);
            }
        }

        public IEnumerable<Role> AvailableRoles
        {
            get
            {
                return GetField<IEnumerable<Role>>();
            }
            set
            {
                SetField(value);
            }
        }

        public Role SelectedAddRole
        {
            get
            {
                return GetField<Role>();
            }
            set
            {
                SetField(value);
            }
        }

        public string SelectedRemoveRole
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand RefreshUsersCommand => GetCommand(RefreshUsers);

        public ICommand RemoveRoleCommand => GetCommand(RemoveRole, CanRemoveRole);

        public ICommand AddRoleCommand => GetCommand(AddRole, CanAddRole);

        public UsersViewModel()
            : base(false)
        {
        }

        private void AddRole()
        {
            // add the role
        }

        private bool CanAddRole()
        {
            return SelectedAddRole != null;
        }

        private void RefreshUsers()
        {
            Users = Client.UserClient
                .Retrieve()
                .Select(u => Convert(u))
                .ToList();
        }

        private bool CanRemoveRole()
        {
            return SelectedRemoveRole != null;
        }

        private void RemoveRole()
        {
            // remove the role
        }

        private static User Convert(UserDto userDto)
        {
            //return new User
            //{
            //    Id = userDto.Id,
            //    UserName = userDto.UserName,
            //    Password = userDto.Password,
            //    EmailAddress = userDto.EmailAddress,
            //    EmailConfirmed = userDto.EmailConfirmed,
            //    PhoneNumber = userDto.PhoneNumber,
            //    PhoneNumberConfirmed = userDto.PhoneNumberConfirmed,
            //    LockoutEnabled = userDto.LockoutEnabled,
            //    LockoutEndDate = userDto.LockoutEndDate,
            //    SecurityStamp = userDto.SecurityStamp,
            //    TwoFactorEnabled = userDto.TwoFactorEnabled,
            //    RoleIds = userDto.RoleIds
            //};
            return null;
        }
    }
}