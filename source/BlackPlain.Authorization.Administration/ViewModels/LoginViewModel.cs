﻿using BlackPlain.Authorization.Administration.Views;
using System.Windows.Input;

namespace BlackPlain.Authorization.Administration.ViewModels
{
    public class LoginViewModel : AuthorizedViewModelBase
    {
        public string Username
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Password
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public string Error
        {
            get
            {
                return GetField<string>();
            }
            set
            {
                SetField(value);
            }
        }

        public ICommand LoginCommand => GetCommand(Login, CanLogin);

        private void Login()
        {
            if (!Login(Username, Password))
            {
                Error = "Invalid username or login";

                return;
            }

            NavigationService.ChangeView(new UsersView(), new UsersViewModel());
        }

        private bool CanLogin()
        {
            return
                !string.IsNullOrWhiteSpace(Username) &&
                !string.IsNullOrWhiteSpace(Password) &&
                Username.Length > 3 &&
                Password.Length > 3;
        }
    }
}