﻿using BlackPlain.Authorization.Administration.Views;
using BlackPlain.Wpf.Core;
using System.Windows;

namespace BlackPlain.Authorization.Administration.ViewModels
{
    public class MainViewModel : NotifyOnPropertyChanged
    {
        public FrameworkElement View
        {
            get
            {
                return GetField<FrameworkElement>();
            }
            set
            {
                SetField(value);
            }
        }

        public object ViewModel
        {
            get
            {
                return GetField<object>();
            }
            set
            {
                SetField(value);
            }
        }

        public MainViewModel()
        {
            NavigationService.ViewChanged += OnViewChanged;

            NavigationService.ChangeView(new LoginView(), new LoginViewModel());
        }

        private void OnViewChanged(object sender, ViewChangedEventArgs e)
        {
            View = e.View;

            ViewModel = e.ViewModel;
        }
    }
}