﻿using BlackPlain.Authorization.Client;
using BlackPlain.Authorization.Models;
using BlackPlain.Wpf.Core;
using System;
using System.Configuration;

namespace BlackPlain.Authorization.Administration.ViewModels
{
    public class AuthorizedViewModelBase : ViewModelBase
    {
        static AuthorizedViewModelBase()
        {
            Client = new AuthorizationClient(ConfigurationManager.AppSettings["AuthorizationApiUri"]);

            _clientId = int.Parse(ConfigurationManager.AppSettings["ClientId"]);

            _clientSecret = ConfigurationManager.AppSettings["ClientSecret"];
        }

        protected AuthorizedViewModelBase()
            : base(true)
        {
        }

        protected bool Login(string username, string password)
        {
            var response = Client.UserClient.GetToken(new GetTokenRequest(_clientId, _clientSecret, username, password));

            if (response == null)
            {
                return false;
            }

            _response = response.Value;

            return true;
        }

        protected TReturn MakeAuthorizedCall<TReturn>(Func<string, TReturn> method)
        {
            var result = method(_response.AccessToken);

            if (result.ResponseCode == )
        }

        protected static IAuthorizationClient Client { get; }

        private static GetTokenResponse _response;

        private static readonly int _clientId;

        private static readonly string _clientSecret;
    }
}