﻿using System;
using System.Windows;

namespace BlackPlain.Authorization.Administration
{
    public class ViewChangedEventArgs : EventArgs
    {
        public FrameworkElement View { get; }

        public object ViewModel { get; }

        public ViewChangedEventArgs(FrameworkElement view, object viewModel)
        {
            View = view;

            ViewModel = viewModel;
        }
    }

    public static class NavigationService
    {
        public static event EventHandler<ViewChangedEventArgs> ViewChanged;

        public static void ChangeView(FrameworkElement view, object viewModel)
        {
            ViewChanged?.Invoke(null, new ViewChangedEventArgs(view, viewModel));
        }
    }
}