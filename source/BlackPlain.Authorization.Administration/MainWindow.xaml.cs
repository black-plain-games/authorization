﻿using BlackPlain.Wpf.Core;

namespace BlackPlain.Authorization.Administration
{
    public partial class MainWindow
    {
        public MainWindow()
        {
            InitializeComponent();

            NotifyOnPropertyChanged.SetUI();
        }
    }
}