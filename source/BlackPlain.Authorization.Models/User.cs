﻿using System;

namespace BlackPlain.Authorization.Web
{
    public class User
    {
        public int Id { get; set; }

        public DateTimeOffset DateCreated { get; set; }

        public DateTimeOffset DateModified { get; set; }

        public string UserName { get; set; }

        public string EmailAddress { get; set; }

        public bool? IsEmailAddressVerified { get; set; }

        public string PhoneNumber { get; set; }

        public bool? IsPhoneNumberVerified { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }
    }
}