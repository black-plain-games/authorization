﻿using System.ComponentModel.DataAnnotations;

namespace BlackPlain.Authorization.Web
{
    public class ResetPasswordRequest
    {
        [Required]
        [StringLength(128)]
        public string UserName { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}