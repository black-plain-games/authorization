﻿using System;

namespace BlackPlain.Authorization.Web
{
    public class UpdateClientRequest
    {
        public string Name { get; set; }

        public bool IsActive { get; set; }

        public string Secret { get; set; }

        public TimeSpan RefreshTokenLifeTime { get; set; }

        public string AllowedOrigin { get; set; }
    }
}