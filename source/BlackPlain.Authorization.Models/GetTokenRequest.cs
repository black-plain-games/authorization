﻿using System;

namespace BlackPlain.Authorization.Web
{
    public struct GetTokenRequest
    {
        public int ClientId;

        public string ClientSecret;

        public string UserName;

        public string Password;

        public string RefreshToken;

        public GetTokenRequest(int clientId, string clientSecret, string userName, string password)
        {
            if (clientId <= 0)
            {
                throw new ArgumentException(nameof(clientId));
            }

            if (string.IsNullOrWhiteSpace(clientSecret))
            {
                throw new ArgumentException(nameof(clientSecret));
            }

            if (string.IsNullOrWhiteSpace(userName))
            {
                throw new ArgumentException(nameof(userName));
            }

            if (string.IsNullOrWhiteSpace(password))
            {
                throw new ArgumentException(nameof(password));
            }

            ClientId = clientId;

            ClientSecret = clientSecret;

            UserName = userName;

            Password = password;

            RefreshToken = null;
        }

        public GetTokenRequest(int clientId, string clientSecret, string refreshToken)
        {
            if (clientId <= 0)
            {
                throw new ArgumentException(nameof(clientId));
            }

            if (string.IsNullOrWhiteSpace(clientSecret))
            {
                throw new ArgumentException(nameof(clientSecret));
            }

            if (string.IsNullOrWhiteSpace(refreshToken))
            {
                throw new ArgumentException(nameof(refreshToken));
            }

            ClientId = clientId;

            ClientSecret = clientSecret;

            UserName = null;

            Password = null;

            RefreshToken = refreshToken;
        }
    }
}