﻿using System.ComponentModel.DataAnnotations;

namespace BlackPlain.Authorization.Web
{
    public class GeneratePasswordResetTokenRequest
    {
        [Required]
        [StringLength(128)]
        public string UserName { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }
    }
}