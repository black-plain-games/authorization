﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BlackPlain.Authorization.Web
{
    public class CreateClientRequest
    {
        [Required]
        [StringLength(128)]
        public string Name { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        public string Secret { get; set; }

        [Required]
        public TimeSpan RefreshTokenLifeTime { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 1)]
        public string AllowedOrigin { get; set; }
    }
}