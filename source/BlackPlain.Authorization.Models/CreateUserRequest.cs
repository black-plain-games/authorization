﻿using System.ComponentModel.DataAnnotations;

namespace BlackPlain.Authorization.Web
{
    public class CreateUserRequest
    {
        [Required]
        [StringLength(128)]
        public string UserName { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [StringLength(15)]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(128, MinimumLength = 1)]
        public string FirstName { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(128, MinimumLength = 1)]
        public string LastName { get; set; }
    }
}