﻿using Newtonsoft.Json;
using System;

namespace BlackPlain.Authorization.Web
{
    public struct GetTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken;

        [JsonProperty("token_type")]
        public string TokenType;

        [JsonProperty("expires_in")]
        public int ExpiresIn;

        [JsonProperty("refresh_token")]
        public string RefreshToken;

        [JsonProperty("client_id")]
        public int ClientId;

        [JsonProperty("user_id")]
        public int? UserId;

        [JsonProperty(".issued")]
        public DateTimeOffset IssueDate;

        [JsonProperty(".expires")]
        public DateTimeOffset ExpirationDate;
    }
}