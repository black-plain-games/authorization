﻿using System.ComponentModel.DataAnnotations;

namespace BlackPlain.Authorization.Web
{
    public class ChangePasswordRequest
    {
        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string CurrentPassword { get; set; }

        [Required]
        [StringLength(128, MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }
    }
}